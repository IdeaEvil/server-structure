var fs = require('fs');
var os = require("os");
var config = null;
var cfg = require('../config/config');
var hostname = cfg.host.name;

function SutisLog() {
}

function getDateTimeFormat ()
{

    var years = new Date().getFullYear();
    var months = new Date().getMonth()+1;
    var day = new Date().getDate();
    var hours = new Date().getHours();
    var mins = new Date().getMinutes();
    var monthFormatted = months < 10 ? "0" + months : months;
    var dayFormatted = day < 10 ? "0" + day : day;
    var hourFormatted = hours < 10 ? "0" + hours : hours;
    var result = "";
    var minFormatted = null;
    var div = null;

    if ( ( mins % cfg.log.rotate ) > 0 )
    {
        minFormatted = ((Math.floor(mins / cfg.log.rotate))*cfg.log.rotate);
    }
    else
    {
        minFormatted = mins;
    }
    minFormatted = minFormatted < 10 ? "0" + minFormatted : minFormatted;
    result = ''+years+monthFormatted+dayFormatted+hourFormatted+minFormatted;
    return result;
}

function getDateTimeLogFormat()
{
    var dates = new Date();
    var years = dates.getFullYear();
    var months = dates.getMonth()+1;
    var day = dates.getDate();
    var hours = dates.getHours();
    var minutes = dates.getMinutes();
    var second = new Date().getSeconds();
    var millisecs = dates.getMilliseconds();
    var monthFormatted = months < 10 ? "0" + months : months;
    var dayFormatted = day < 10 ? "0" + day : day;
    var hourFormatted = hours < 10 ? "0" + hours : hours;
    var minFormatted = minutes < 10 ? "0" + minutes : minutes;
    var secFormatted = second < 10 ? "0" + second : second;
    var milliFormatted = null;

    if ( millisecs < 10 )
    {
        milliFormatted = "00" + millisecs;
    }
    else if ( millisecs < 100 )
    {
        milliFormatted = "0" + millisecs;
    }
    else
    {
        milliFormatted = millisecs;
    }

    return '['+years+'-'+monthFormatted+'-'+dayFormatted+' '+hourFormatted+':'+minFormatted+':'+secFormatted+':'+milliFormatted+']';
}

function getLogFileName()
{
    return hostname+'_'+cfg.log.projectName+'_'+getDateTimeFormat()+'.log';
}

SutisLog.prototype.info = function (logMessage)
{
    var stream = fs.createWriteStream(cfg.log.path+'/'+getLogFileName(), {'flags': 'a'});
    stream.once('open', function(fd) {
        stream.write(getDateTimeLogFormat()+' - info: '+logMessage+'\n');
        stream.end();
    });
};

SutisLog.prototype.writeInfo = function (logMessage)
{
    var stream = fs.createWriteStream(cfg.log.path+'/'+getLogFileName(), {'flags': 'a'});
    stream.once('open', function(fd) {
        stream.write(logMessage+'\n');
        stream.end();
    });
};

SutisLog.prototype.writeError = function (logMessage)
{
    var stream = fs.createWriteStream(cfg.log.path+'/'+getLogFileName(), {'flags': 'a'});
    stream.once('open', function(fd) {
        stream.write(logMessage+'\n');
        stream.end();
    });
};

SutisLog.prototype.error = function(logMessage)
{
    var stream = fs.createWriteStream(cfg.log.path+'/'+getLogFileName(), {'flags': 'a'});

    stream.once('open', function(fd) {
        stream.write(getDateTimeLogFormat()+' - error: '+logMessage+'\n');
        stream.end();
    });
};

module.exports = SutisLog;

