'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_NotificationsSchema  = new Schema({
    platform            : {type: String, default: null},
    pns_token           : {type: String, default: null},
    pns_key             : {type: String, default: null},
    pns_packge          : {type: String, default: null}
});

var Infomation_Notifications = mongoose.model('Infomation_Notifications', Infomation_NotificationsSchema, 'Infomation_Notifications');
module.exports = Infomation_Notifications;
