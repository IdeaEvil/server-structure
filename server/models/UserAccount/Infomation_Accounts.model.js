'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_AccountsSchema  = new Schema({

    username                : {type: String, required: true, unique: true},
    password                : {type: String, required: true },
    authe                   : {
        x_access_token      : {type: String, default: null},
        token_expired       : {type: Number, default: 0},
        status              : {type: Number, default: 0}, // 0 = reserver , 1 = enlable , 2 = disable
        state               : {type: Number, default: 0}, // 0 = logout, 1 = login
        last_login          : {type: Date,   default: null},
    },
    usertype                : [{type: Schema.ObjectId, ref: 'Infomation_Companys'}],
    userinfo                : {type: Schema.ObjectId, ref: 'Infomation_Profiles', required: true},
    notification            : {type: Schema.ObjectId, ref: 'Infomation_Notifications', required: true},

    company_id              : {type: Schema.ObjectId, ref: 'Infomation_Companys'},


});

var Infomation_Accounts = mongoose.model('Infomation_Accounts', Infomation_AccountsSchema, 'Infomation_Accounts');
module.exports = Infomation_Accounts;
