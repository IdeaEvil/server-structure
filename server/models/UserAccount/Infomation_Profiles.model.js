'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_ProfilesSchema  = new Schema({
    firstname           : {type: String, default: null},
    lastname            : {type: String, default: null},
    image_profile       : {type: String, default: null},
    card_id             : {type: String, default: null},
    age                 : {type: Number, default: 0},
    birthday            : {type: Date, required: true},
    mobile              : {type: String, required: true, unique: true},
    email               : {type: String, required: true, unique: true},
    lineid              : {type: String, default: null},
    address             : {
        address         : {type: String, default: null},
        zipcode         : {type: String, default: null}
    },
    driver_licenses     : [{type: Schema.ObjectId, ref: 'Driver_Licenses'}]
});

var Infomation_Profiles = mongoose.model('Infomation_Profiles', Infomation_ProfilesSchema, 'Infomation_Profiles');
module.exports = Infomation_Profiles;
