'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_AuthorizationSchema  = new Schema({
    title           : {type: String, required: true},
    description     : {type: String, default: null},
    service         : {type: String, required: true},
    type_access     : [{type: Schema.ObjectId, ref: 'Infomation_UserType'}],
    create          : {
        by          : {type: Schema.ObjectId, ref: 'Infomation_Profiles', required: true},
        date        : {type: Date, required: true},
    },
    update          : [{
        by          : {type: Schema.ObjectId, ref: 'Infomation_Profiles', required: true},
        date        : {type: Date, required: true},
    }]
});

var Infomation_Authorization = mongoose.model('Infomation_Authorizations', Infomation_AuthorizationSchema, 'Infomation_Authorizations');
module.exports = Infomation_Authorization;
