'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_AuthorizationSchema  = new Schema({
    user            : {type: String, required: true, unique: true},
    password        : {type: String, required: true},
    owner_service   : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    api_service     : {
        use_count   : {type: Number, default: 0},
        use_max     : {type: Number, default: 0},
        expired     : {type: Date, required: true},
        allow       : [{type: Schema.ObjectId, ref: 'Infomation_Apis'}]

    },
    create          : {
        by          : {type: Schema.ObjectId, ref: 'Infomation_Profiles', required: true},
        date        : {type: Date, required: true},
    },
    update          : [{
        by          : {type: Schema.ObjectId, ref: 'Infomation_Profiles', required: true},
        date        : {type: Date, required: true},
    }]



});

var Infomation_Authorization = mongoose.model('Infomation_Authorizations', Infomation_AuthorizationSchema, 'Infomation_Authorizations');
module.exports = Infomation_Authorization;
