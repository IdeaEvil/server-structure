'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_TransactionOtpSchema  = new Schema({
    username                : {type: String, default: null},
    userinfo                : {type: Schema.ObjectId, ref: 'Infomation_Profiles', required: true},
    otp                     : {type: String, default: null},
    verificationcode        : {type: String, default: null},
    type                    : {type: Number, default: 0},
    transaction             : {type: String, default: null},
    transactionExpired      : {type: Number, default: 0}
});

var Infomation_TransactionOtp = mongoose.model('Infomation_TransactionOtps', Infomation_TransactionOtpSchema, 'Infomation_TransactionOtps');
module.exports = Infomation_TransactionOtp;
