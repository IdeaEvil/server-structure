var auth = require('../../middlewares/authentication');
var paramValidate = require('../../middlewares/parametersValidator');
var utils = require('../../helpers/utils');
var resMsg = require('../../config/message');

/* Controller */
var Service_Applications = require('../controllers/Service_Applications');


module.exports = function (app) {
    var path = '/api/v1/';

    /*----- Check Version API -----*/
    app.get(path + 'versions/:platform', auth.authenApi, Service_Applications.checkVersion);


    /*----- Catch 404 Error -----*/
    app.use(function (req, res) {
        res.status(404).json(resMsg.getMsg(40400));
        utils.writeLog('Client Request', req, utils.getTimeInMsec(), '----- Request Error URL[' + req.url + '] Status[' + JSON.stringify(resMsg.getMsg(40400)) + '] -----');
    });

    /*----- Catch 500 Error -----*/
    app.use(function (err, req, res) {
        res.status(500).json(resMsg.getMsg(50000));
        utils.writeLog('Client Request', req, utils.getTimeInMsec(), '----- Request Error URL[' + req.url + '] Status[' + JSON.stringify(resMsg.getMsg(50000)) + '] -----', err);
    });
}
