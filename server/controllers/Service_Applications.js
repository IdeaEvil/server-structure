var resMsg = require('../../config/message');
var utils = require('../../helpers/utils');
var Versions = require('../models/UtilApplication/Infomation_Versions');

exports.checkVersion = function (request, response) {
    var startTime = utils.getTimeInMsec();
    try {
        var query = {};
        if (request.params.platform){
            query.platform = request.params.platform;
        }
        Versions
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    response.status(500).json(resMsg.getMsg(50002));
                    utils.writeLog('checkVersion', req, startTime, resMsg.getMsg(50002), err);
                } else {
                    var resData = resMsg.getMsg(20000);
                    resData.data = doc;
                    response.json(resData);
                    utils.writeLog('checkVersion', request, startTime, resMsg.getMsg(200));
                }
            });
    } catch (err) {
        response.status(500).json(resMsg.getMsg(50000));
        utils.writeLog('checkVersion', request, startTime, resMsg.getMsg(50000), err);
    }
}

exports.onService_onTermsAndConditions = function (request, response) {

    var command = 'onService_onTermsAndConditions';
    var startTime = utils.getTimeInMsec();
    var resData = resMsg.getMsg(20000);
    resData.data = { "link":"http://google.com"};
    response.status(200).json(resData);
    // utils.writeLog(command, request, startTime, resMsg.getMsg(20000), err);
}
