'use strict';

var resMsg = require('../config/message');
var utils = require('../helpers/utils');
// var ObjectId = require('mongoose').Types.ObjectId;

exports.checkUsernameValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkUsername');
    } else {
        next();
    }
};

exports.checkMobileValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobile', 'mobile is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkMobile');
    } else {
        next();
    }
};

exports.checkEmailValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('email', 'email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkEmail');
    } else {
        next();
    }
};

exports.checkRegisterValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    req.checkBody('mobile', 'mobile is required').notEmpty();
    req.checkBody('email', 'email is required').notEmpty();
    req.checkBody('password', 'password is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkRegister');
    } else {
        next();
    }
};

exports.checkRequestSMSValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkUsername');
    } else {
        next();
    }
};

exports.checkConfirmSMSValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    req.checkBody('transaction', 'transaction is required').notEmpty();
    req.checkBody('otp', 'otp is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkUsername');
    } else {
        next();
    }
};

exports.checkForgotPasswordValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    // req.checkBody('transaction', 'transaction is required').notEmpty();
    req.checkBody('otp', 'otp is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkUsername');
    } else {
        next();
    }
};

exports.checkForgotPasswordConfirmValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    req.checkBody('transaction', 'transaction is required').notEmpty();
    req.checkBody('password', 'password is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkUsername');
    } else {
        next();
    }
};

exports.checkPassowrdValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('password', 'password is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkMobile');
    } else {
        next();
    }
};

exports.checkAuthensValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('username', 'username is required').notEmpty();
    req.checkBody('password', 'password is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkRegister');
    } else {
        next();
    }
};


function missingOrInvalidRes(req, res, startTime, method) {
    res.status(400).json(resMsg.getMsg(40300));
    utils.writeLog(method, req, startTime, resMsg.getMsg(40300));
}
