'use strict';

var resMsg = require('../config/message');
// var utils = require('../helpers/utils');
// var ObjectId = require('mongoose').Types.ObjectId;

exports.checkRegisterValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();
    // req.checkBody('email', 'Emai is required').notEmpty().isEmail();
    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkRegister');
    } else {
        next();
    }
};

exports.registerFarmerValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    req.checkBody('name', 'name is required').notEmpty();
    req.checkBody('address', 'address is required').notEmpty();
    req.checkBody('birthDay', 'birthDay is required').notEmpty();
    req.checkBody('password', 'password is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'registerFarmer');
    } else {
        next();
    }
};

exports.registerFarmerFromFarmValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    req.checkBody('updateMobileNo', 'updateMobileNo is required').notEmpty();
    req.checkBody('name', 'name is required').notEmpty();

    req.checkBody('address', 'address is required').notEmpty();
    req.checkBody('birthDay', 'birthDay is required').notEmpty();
    req.checkBody('productCategory', 'productCategory is required').notEmpty();
    req.checkBody('farmDesc', 'farmDesc is required').notEmpty();


    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'registerFarmerFromFarm');
    } else {
        next();
    }
};

exports.loginFarmerValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    req.checkBody('password', 'password is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'loginFarmer');
    } else {
        next();
    }
};

exports.resetPassFarmerValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    req.checkBody('password', 'password is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'resetPassFarmer');
    } else {
        next();
    }
};

exports.changePassFarmerValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    req.checkBody('oldPassword', 'password is required').notEmpty();
    req.checkBody('newPassword', 'password is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'changePassFarmer');
    } else {
        next();
    }
};

exports.getWeatherValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('location', 'location is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'getWeather');
    } else {
        next();
    }
};

exports.getEmergencyNewsValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('provinceId', 'provinceId is required').notEmpty();
    // req.checkBody('userId', 'userId is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'getEmergencyNews');
    } else {
        next();
    }
};

exports.createEmergencyValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('message', 'message is required').notEmpty();
    req.checkBody('provinceId', 'provinceId is required').notEmpty();
    req.checkBody('location', 'location is required').notEmpty();
    req.checkBody('userId', 'userId is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'createNewsFeed');
    } else {
        next();
    }
};

exports.createNewsFeedValidator = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('title', 'title is required').notEmpty();
    req.checkBody('description', 'description is required').notEmpty();
    req.checkBody('imgUrl', 'imgUrl is required').notEmpty();
    req.checkBody('userId', 'userId is required').notEmpty();


    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'createNewsFeed');
    } else {
        next();
    }
};

exports.validId = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('id', 'id is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'validId');
    } else {
        next();
    }
};

exports.checkUpdateTrackingId = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

   req.checkBody('id', 'id is required').notEmpty();
    // req.checkBody('trackingNo', 'trackingNo is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkUpdateTrackingId');
    } else {
        next();
    }
};

exports.checkRegisterOTP = function (req, res, next) {
    var startTime = utils.getTimeInMsec();

    req.checkBody('mobileNo', 'mobileNo is required').notEmpty();
    req.checkBody('otp', 'otp is required').notEmpty();
    // req.checkBody('address', 'address is required').notEmpty();
    req.checkBody('transactionId', 'transactionId is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkRegisterOTP');
    } else {
        next();
    }
};

exports.createProduct = function (req, res, next) {
    var startTime = utils.getTimeInMsec();
                        req.checkBody('categoryId', 'categoryId is required').notEmpty();
                        req.checkBody('productName', 'productName is required').notEmpty();
                        req.checkBody('description', 'description is required').notEmpty();
                        req.checkBody('price', 'price is required').notEmpty();
                        // req.checkBody('unit', 'unit is required').notEmpty();
                        req.checkBody('stock', 'stock is required').notEmpty();
                        req.checkBody('weight', 'weight is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkRegisterOTP');
    } else {
        next();
    }
};

exports.checkUpdateFarmAis = function (req, res, next) {
    var startTime = utils.getTimeInMsec();
                        req.checkBody('merchantId', 'merchantId is required').notEmpty();
                        req.checkBody('farmId', 'farmId is required').notEmpty();
                        req.checkBody('productName', 'productName is required').notEmpty();
                        req.checkBody('description', 'description is required').notEmpty();
                        req.checkBody('price', 'price is required').notEmpty();
                        req.checkBody('unit', 'unit is required').notEmpty();
                        req.checkBody('stock', 'stock is required').notEmpty();
                        req.checkBody('weight', 'weight is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkRegisterOTP');
    } else {
        next();
    }
};

exports.checkMonth = function (req, res, next) {
    var startTime = utils.getTimeInMsec();
                        req.checkBody('month', 'month is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkMonth');
    } else {
        next();
    }
};


exports.checkonVerifyCode_Send = function (req, res, next) {
    var startTime = utils.getTimeInMsec();
    req.checkBody('phonenumber', 'phonenumber is required').notEmpty();
    req.checkBody('code', 'code is required').notEmpty();
    req.checkBody('email', 'email is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        missingOrInvalidRes(req, res, startTime, 'checkonVerifyCode_Send');
    } else {
        next();
    }
};


function missingOrInvalidRes(req, res, startTime, method) {
    res.status(400).json(resMsg.getMsg(40300));
    utils.writeLog(method, req, startTime, resMsg.getMsg(40300));
}
