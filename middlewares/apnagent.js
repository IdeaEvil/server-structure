'use strict';

var cfg = require('../config/config');
var join = require('path').join;
var apnagent = require('apnagent')
    , agent = module.exports = new apnagent.Agent();

agent
    .set('cert file', join(__dirname,cfg.certPushIos.cert))
    .set('key file', join(__dirname,cfg.certPushIos.key))
    .set('passphrase', cfg.certPushIos.passphrase);

// agent.set('pfx file', join(__dirname,cfg.getcertPushIos().cert))
//     .enable('sandbox');


agent.connect(function (err) {
    // console.log("connect apn");
    // gracefully handle auth problems
    if (err) {
        console.log('Authentication Error: %s', err.message);
        process.exit(1);
    }

    // handle any other err (not likely)
    else if (err) {
        throw err;
    }

    // it worked!
    var env = agent.enabled('sandbox')
        ? 'sandbox'
        : 'production';

    console.log('apnagent [%s] gateway connected', env);
});