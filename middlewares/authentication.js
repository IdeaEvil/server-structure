'use strict';

var basicAuth = require('basic-auth');
var cfg = require('../config/config');
var resMsg = require('../config/message');
var utils = require('../helpers/utils');

var https = require('https');
var Infomation_Authorizations = require('../server/models/UtilApplication/Infomation_Authorization.model');
// var Infomation_Accounts = require('../server/models/Profiles/Infomation_Accounts.model');

// var Farmers = require('../server/models/farmer.model.js');
// var Staffs = require('../server/models/staff.model');
// var utils = require('../helpers/utils');
// var ObjectId = require('mongoose').Types.ObjectId;


// exports.onService_AllowforAdmin = function (request, response, next) {
//     var command = 'onService_AllowforAdmin';
//     var startTime = utils.getTimeInMsec();
//
//     var query = {};
//     query.x_access_token = request.headers.x_access_token;
//     Infomation_Accounts
//         .findOne(query)
//         .lean()
//         .exec(function (err, doc) {
//             if(doc != null){
//                 switch(doc.usertype) {
//                     case 0:
//                         return next();
//                         break;
//                     default:
//                         response.status(401).json(resMsg.getMsg(40103));
//                         utils.writeLog(401, request, startTime, resMsg.getMsg(40103), err);
//                         break;
//                 }
//             }else{
//                 response.status(401).json(resMsg.getMsg(40102));
//                 utils.writeLog(401, request, startTime, resMsg.getMsg(40102), err);
//             }
//         });
// }
//
// exports.onService_AllowforSupplier = function (request, response, next) {
//     var command = 'onService_AllowforAdminandSupplier';
//     var startTime = utils.getTimeInMsec();
//
//     var query = {};
//     query.x_access_token = request.headers.x_access_token;
//     Infomation_Accounts
//         .findOne(query)
//         .lean()
//         .exec(function (err, doc) {
//             if(doc != null){
//                 switch(doc.usertype) {
//                     case 0:
//                         return next();
//                         break;
//                     case 2:
//                         return next();
//                         break;
//                     default:
//                         response.status(401).json(resMsg.getMsg(40103));
//                         utils.writeLog(401, request, startTime, resMsg.getMsg(40103), err);
//                         break;
//                 }
//             }else{
//                 response.status(401).json(resMsg.getMsg(40102));
//                 utils.writeLog(401, request, startTime, resMsg.getMsg(40102), err);
//             }
//         });
// }
//
// exports.onService_Expired = function (request, response, next) {
//     var command = 'onService_Expired';
//     var startTime = utils.getTimeInMsec();
//
//     var query = {};
//     query.x_access_token = request.headers.x_access_token;
//     Infomation_Accounts
//         .findOne(query)
//         .lean()
//         .exec(function (err, doc) {
//             if(doc != null){
//                 if(doc.tokenExpired > utils.getTimeInMsec()){
//                     request.headers._id = doc._id;
//                     request.headers.username = doc.username;
//                     request.headers.usertype = doc.usertype;
//                     request.headers.company_Id = doc.companyId;
//                     next();
//                 }else{
//                     response.status(401).json(resMsg.getMsg(40101));
//                     utils.writeLog(command, request, startTime, resMsg.getMsg(40101), err);
//                 }
//             }else{
//                 response.status(401).json(resMsg.getMsg(40102));
//                 utils.writeLog(401, request, startTime, resMsg.getMsg(40102), err);
//             }
//         });
// }


exports.authenApi = function (req, res, next) {
    function unauthorized(res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(resMsg.getMsg(401));
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
        return unauthorized(res);
    };


    var query = {};
    query.user = user.name;
    query.password = user.pass;

    Infomation_Authorizations
        .findOne(query)
        .lean()
        .exec(function (err, doc) {
            if(doc != null ){
                return next();
            }else{
                return unauthorized(res);
            }
        });
};

// exports.authenFb = function (fbId, inputToken, cb) {
//
//     var options = {
//         host: cfg.fbAuth.host,
//         path: cfg.fbAuth.path + cfg.fbAuth.param.replace("{input_token}", inputToken),
//         port: cfg.fbAuth.port,
//         method: cfg.fbAuth.method
//     };
//
//     // Set up the request
//     var get_req = https.request(options, function (res) {
//         res.setEncoding('utf8');
//         res.on('data', function (data) {
//             var fbAuth = JSON.parse(data);
//
//             if (fbAuth.error) {
//                 return cb(false);
//             }
//             else if (typeof fbAuth.data.user_id === "undefined") {
//                 return cb(false);
//             }
//             else if (fbAuth.data.user_id != fbId) {
//                 return cb(false);
//             }
//             return cb(true);
//
//         });
//     });
//
//     get_req.end();
// };

// exports.checkUser = function (req, res, next) {
//     var startTime = utils.getTimeInMsec();
//
//     try{
//         var userId = null;
//         if (req.body.userId){
//             userId = req.body.userId;
//         }else if (req.params.userId){
//             userId = req.params.userId;
//         }
//         // var query = ;
//
//         Users
//             .findOne({_id: new ObjectId(userId)})
//             .exec(function (err, userObj) {
//                 if (err) {
//                     res.status(500).json(resMsg.getMsg(50002));
//                     utils.writeLog('checkUser', req, startTime, resMsg.getMsg(50002), err);
//                 } else {
//                     if (!userObj) {
//                         res.json(resMsg.getMsg(40402));
//                         utils.writeLog('checkUser', req, startTime, resMsg.getMsg(40402));
//                     } else {
//                         req.body.userObj = userObj;
//                         next();
//                     }
//                 }
//             })
//     }catch (err) {
//         res.status(500).json(resMsg.getMsg(50000));
//         utils.writeLog('checkUser', req, startTime, resMsg.getMsg(50000), err);
//     }
// };

// exports.checkUser = function (req, res, next) {
//     var startTime = req.body.startTime;
//     var command = 'checkUser';
//
//     try{
//         if (!req.get("x-access-token")){
//             res.json(resMsg.getMsg(40300));
//             utils.writeLog(command, req, startTime, resMsg.getMsg(40300));
//         }else{
//             // console.log(req.get("x-access-token"));
//             Farmers
//                 .findOne({accessToken: req.get("x-access-token")})
//                 .exec(function (err, userObj) {
//                     // console.log(userObj)
//                     if (err) {
//                         res.status(500).json(resMsg.getMsg(50002));
//                         utils.writeLog('checkUser', req, startTime, resMsg.getMsg(50002), err);
//                     } else {
//                         if (!userObj) {
//                             res.json(resMsg.getMsg(40402));
//                             utils.writeLog('checkUser', req, startTime, resMsg.getMsg(40402));
//                         } else {
//                             if ( utils.getTimeInMsec() < userObj.tokenExpired ) {
//                                 req.user = userObj;
//                                 next();
//                             } else {
//                                 res.json(resMsg.getMsg(40102));
//                                 utils.writeLog('checkUser', req, startTime, resMsg.getMsg(40102));
//                             }
//                         }
//                     }
//                 })
//         }
//     }catch (err) {
//         res.status(500).json(resMsg.getMsg(50000));
//         utils.writeLog(command, req, startTime, resMsg.getMsg(50000), err);
//     }
// };

// exports.checkAdmin= function (req, res, next) {
    // var startTime = utils.getTimeInMsec();
    // try{
    //     var query = {};
    //     query.accessToken = req.get("x-access-token");
    //     Staffs
    //         .findOne(query)
    //         .lean()
    //         .exec(function (err, staffObj) {
    //             if (err){
    //                 res.status(500).json(resMsg.getMsg(50002));
    //                 utils.writeLog('checkAdmin', req, startTime, resMsg.getMsg(50002), err);
    //             }else{
    //                 if (!staffObj){
    //                     res.json(resMsg.getMsg(40402));
    //                     utils.writeLog('checkAdmin', req, startTime, resMsg.getMsg(40402));
    //                 }else{
    //                     req.body.staffObj = staffObj;
    //                     next();
    //                 }
    //             }
    //         });
    // }catch (err) {
    //     res.status(500).json(resMsg.getMsg(50000));
    //     utils.writeLog('checkAdmin', req, startTime, resMsg.getMsg(50000), err);
    // }
// }
